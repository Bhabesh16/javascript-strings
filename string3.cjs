function string3(date) {
    if (typeof date !== 'string') {
        return 0;
    }

    const numbers = date.split('/');
    const month = parseInt(numbers[1]);

    return isNaN(month)? 0 : month;

}

module.exports = string3;