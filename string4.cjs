function toTitleCase(name) {
    name = name.toLowerCase();
    name = name[0].toUpperCase() + name.slice(1);

    return name;
}

function string4(objectName){
    if (typeof objectName !== 'object' || objectName.first_name === undefined || objectName.last_name === undefined)  {
        return [];
    }

    let fullName = "";

    if (objectName.middle_name === undefined) {
        fullName = toTitleCase(objectName.first_name) + " " + toTitleCase(objectName.last_name);
    }
    else {
        fullName = toTitleCase(objectName.first_name) + " " + toTitleCase(objectName.middle_name) + " " + toTitleCase(objectName.last_name);
    }

    return fullName;

}

module.exports = string4;