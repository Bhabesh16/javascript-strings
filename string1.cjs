function string1(number) {
    if (typeof number !== 'string') {
        return 0;
    }

    let newNumber = number.replace('$', '');
    newNumber = newNumber.replaceAll(',', '');
    newNumber = newNumber.replaceAll(' ', '');
    newNumber = parseFloat(newNumber);

    return isNaN(newNumber) ? 0 : parseFloat(newNumber);

}

module.exports = string1;