function string5(stringArray) {
    if (typeof stringArray !== 'object' || JSON.stringify(stringArray)[0] == '{') {
        return "";
    }

    let newString = stringArray.toString();
    newString = newString.replaceAll(',', ' ');

    return newString;

}

module.exports = string5;